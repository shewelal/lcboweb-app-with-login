package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginLengthRegular( ) {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "shewelalec" ) );
	}
	@Test
	public void testIsValidLoginLengthBoundaryIn() {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "shewel" ) );
	}
	@Test
	public void testIsValidLoginLengthBoundaryOut() {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "shewe" ) );
	}
	@Test (expected=NullPointerException.class)
	public void testIsValidLoginLengthException() {
		boolean isValid = LoginValidator.isValidLoginName(null);
		fail("incorrect login name");
	}
	
	@Test
	public void testIsValidLoginCharactersRegular() {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "shewe64lal" ) );
	}
	@Test
	public void testIsValidLoginCharactersBoundaryIn() {
		assertTrue("Invalid login" , LoginValidator.isValidLoginName( "shewelal6" ) );
	}
	@Test
	public void testIsValidLoginCharactersBoundaryOut() {
		assertFalse("Invalid login" , LoginValidator.isValidLoginName( "shewel&" ) );
	}
	
	@Test (expected=NullPointerException.class)
	public void testIsValidLoginCharactersException() {
		boolean isValid = LoginValidator.isValidLoginName(null);
		fail("incorrect login name");
	}

}
