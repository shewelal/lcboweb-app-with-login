package sheridan;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		int l = loginName.length();
		for(int i = 0; i < l; i++) {
			if ((Character.isLetterOrDigit(loginName.charAt(i)) == false)) {
	            return false;
	         }
		}
		
		if(loginName.length() < 6) {
			return false;
		}
		
		return true ;
	}
}
